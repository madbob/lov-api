LovAPI
======

Very simple wrapper to the Linked Open Vocabularies (LOV) API.

```
use MadBob\LovAPI\LovAPI;

/*
    Inits the client
*/
$lov = new LovAPI;

/*
    Fetches some information about the ontology, given his namespace
*/
$info = $lov->get('http://schema.org/');

/*
    Fetches the actual ontology file from the repository, given a namespace
*/
$file = $lov->fetchFile('http://schema.org/');
```
