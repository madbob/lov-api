<?php

namespace MadBob\LovAPI;

class LovAPI
{
    private $api_prefix = 'https://lov.linkeddata.es/dataset/lov/api/v2/';
    private $client = null;
    private $index = null;

    private function getBasicClient()
    {
        if (is_null($this->client)) {
            $this->client = new \GuzzleHttp\Client();
        }

        return $this->client;
    }

    private function rawGet($url, $params = [])
    {
        $client = $this->getBasicClient();
        $res = $client->request('GET', $url, ['query' => $params]);
        return $res->getBody();
    }

    private function doGet($url, $params = [])
    {
        $url = $this->api_prefix . $url;
        return json_decode($this->rawGet($url, $params));
    }

    private function getIndex()
    {
        if (is_null($this->index)) {
            $this->index = $this->doGet('vocabulary/list');
        }

        return $this->index;
    }

    public function get($namespace)
    {
        $index = $this->getIndex();

        foreach($index as $i) {
            if ($i->nsp == $namespace) {
                return $i;
            }
        }

        return null;
    }

    public function fetchFile($namespace)
    {
        $info = $this->get($namespace);
        if (!is_null($info)) {
            $metadata = $this->doGet('vocabulary/info', ['vocab' => $info->prefix]);
            if (!is_null($metadata)) {
                $url = $metadata->versions[0]->fileURL ?? null;
                if ($url) {
                    return $this->rawGet($url);
                }
            }
        }

        return null;
    }
}
